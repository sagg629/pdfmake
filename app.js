/**
 * Main APP definition
 * Created by Sergio A Guzman <sagg629@gmail.com> on 13/11/15 10:57 AM
 */
define(["angularAMD", "angular-route"], function (angularAMD) {
    var app = angular.module("myApp", ["ngRoute"]);
    app.config(function ($routeProvider) {
        $routeProvider
            .when('/', angularAMD.route({
                templateUrl: 'views/main.html',
                controller: 'mainController',
                controllerUrl: 'views/mainController'
            }))
            .when('/v1', angularAMD.route({
                templateUrl: 'views/view1/view1.html',
                controller: 'view1Controller',
                controllerUrl: 'views/view1/view1Controller'
            }))
            .when('/v2', angularAMD.route({
                templateUrl: 'views/view2/view2.html',
                controller: 'view2Controller',
                controllerUrl: 'views/view2/view2Controller'
            }))
            .otherwise({
                templateUrl: 'index.html'
            });
    });
    return angularAMD.bootstrap(app);
});
