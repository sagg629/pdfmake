/**
 * RequireJS config
 * Created by Sergio A Guzman <sagg629@gmail.com> on 11/03/16 10:55 AM
 */
require.config({
    baseUrl: "",
    paths: {
        "jquery": "lib/jquery-2.1.3/jquery-2.1.3.min",
        "angular": "lib/angular-1.3.9/angular.min",
        "angular-route": "lib/angular-1.3.9/angular-route.min",
        "angularAMD": "lib/angularAMD-0.2.1",
        "materialize": "lib/materialize-0.97.3/js/materialize.min",
        "materialize-init": "lib/materialize-0.97.3/js/init",
        "moment": "lib/moment-2.10.6.min"
    },
    shim: {
        "angularAMD": ["angular", "jquery"],
        "angular-route": ["angular", "jquery"],
        "materialize": ["angularAMD"],
        "materialize-init": ["angularAMD"]
    },
    deps: ["app"]
});
