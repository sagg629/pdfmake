/**
 * Created by Sergio A Guzman <sagg629@gmail.com> on 3/03/16 03:18 PM
 */
define(["app"], function (app) {
    app.controller('view2Controller', function ($scope) {
        $scope.msg2 = "view 2";
    });
});

