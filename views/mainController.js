/**
 * Controller of main page
 * Created by Sergio A Guzman <sagg629@gmail.com> on 13/11/15 11:30 AM
 */
define(["app", "views/filters/sigFigs", "views/filters/convertUnits", "views/filters/bedsFilter", "moment"], function (app) {
    app.controller('mainController', function ($scope, $filter) {
        var moment = require("moment");
        $scope.scarabLogoBase64 = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIMAAAAhCAYAAAD6SRiDAAAKJ2lDQ1BpY20AAHjanZZ3VFTXFofPvXd6oc0w0hl6ky4wgPQuIB0EURhmBhjKAMMMTWyIqEBEEREBRZCggAGjoUisiGIhKKhgD0gQUGIwiqioZEbWSnx5ee/l5ffHvd/aZ+9z99l7n7UuACRPHy4vBZYCIJkn4Ad6ONNXhUfQsf0ABniAAaYAMFnpqb5B7sFAJC83F3q6yAn8i94MAUj8vmXo6U+ng/9P0qxUvgAAyF/E5mxOOkvE+SJOyhSkiu0zIqbGJIoZRomZL0pQxHJijlvkpZ99FtlRzOxkHlvE4pxT2clsMfeIeHuGkCNixEfEBRlcTqaIb4tYM0mYzBXxW3FsMoeZDgCKJLYLOKx4EZuImMQPDnQR8XIAcKS4LzjmCxZwsgTiQ7mkpGbzuXHxArouS49uam3NoHtyMpM4AoGhP5OVyOSz6S4pyalMXjYAi2f+LBlxbemiIluaWltaGpoZmX5RqP+6+Dcl7u0ivQr43DOI1veH7a/8UuoAYMyKarPrD1vMfgA6tgIgd/8Pm+YhACRFfWu/8cV5aOJ5iRcIUm2MjTMzM424HJaRuKC/6386/A198T0j8Xa/l4fuyollCpMEdHHdWClJKUI+PT2VyeLQDf88xP848K/zWBrIieXwOTxRRKhoyri8OFG7eWyugJvCo3N5/6mJ/zDsT1qca5Eo9Z8ANcoISN2gAuTnPoCiEAESeVDc9d/75oMPBeKbF6Y6sTj3nwX9+65wifiRzo37HOcSGExnCfkZi2viawnQgAAkARXIAxWgAXSBITADVsAWOAI3sAL4gWAQDtYCFogHyYAPMkEu2AwKQBHYBfaCSlAD6kEjaAEnQAc4DS6Ay+A6uAnugAdgBIyD52AGvAHzEARhITJEgeQhVUgLMoDMIAZkD7lBPlAgFA5FQ3EQDxJCudAWqAgqhSqhWqgR+hY6BV2ArkID0D1oFJqCfoXewwhMgqmwMqwNG8MM2An2hoPhNXAcnAbnwPnwTrgCroOPwe3wBfg6fAcegZ/DswhAiAgNUUMMEQbigvghEUgswkc2IIVIOVKHtCBdSC9yCxlBppF3KAyKgqKjDFG2KE9UCIqFSkNtQBWjKlFHUe2oHtQt1ChqBvUJTUYroQ3QNmgv9Cp0HDoTXYAuRzeg29CX0HfQ4+g3GAyGhtHBWGE8MeGYBMw6TDHmAKYVcx4zgBnDzGKxWHmsAdYO64dlYgXYAux+7DHsOewgdhz7FkfEqeLMcO64CBwPl4crxzXhzuIGcRO4ebwUXgtvg/fDs/HZ+BJ8Pb4LfwM/jp8nSBN0CHaEYEICYTOhgtBCuER4SHhFJBLVidbEACKXuIlYQTxOvEIcJb4jyZD0SS6kSJKQtJN0hHSedI/0ikwma5MdyRFkAXknuZF8kfyY/FaCImEk4SXBltgoUSXRLjEo8UISL6kl6SS5VjJHslzypOQNyWkpvJS2lIsUU2qDVJXUKalhqVlpirSptJ90snSxdJP0VelJGayMtoybDFsmX+awzEWZMQpC0aC4UFiULZR6yiXKOBVD1aF6UROoRdRvqP3UGVkZ2WWyobJZslWyZ2RHaAhNm+ZFS6KV0E7QhmjvlygvcVrCWbJjScuSwSVzcopyjnIcuUK5Vrk7cu/l6fJu8onyu+U75B8poBT0FQIUMhUOKlxSmFakKtoqshQLFU8o3leClfSVApXWKR1W6lOaVVZR9lBOVd6vfFF5WoWm4qiSoFKmclZlSpWiaq/KVS1TPaf6jC5Ld6In0SvoPfQZNSU1TzWhWq1av9q8uo56iHqeeqv6Iw2CBkMjVqNMo1tjRlNV01czV7NZ874WXouhFa+1T6tXa05bRztMe5t2h/akjpyOl06OTrPOQ12yroNumm6d7m09jB5DL1HvgN5NfVjfQj9ev0r/hgFsYGnANThgMLAUvdR6KW9p3dJhQ5Khk2GGYbPhqBHNyMcoz6jD6IWxpnGE8W7jXuNPJhYmSSb1Jg9MZUxXmOaZdpn+aqZvxjKrMrttTjZ3N99o3mn+cpnBMs6yg8vuWlAsfC22WXRbfLS0suRbtlhOWWlaRVtVWw0zqAx/RjHjijXa2tl6o/Vp63c2ljYCmxM2v9ga2ibaNtlOLtdZzllev3zMTt2OaVdrN2JPt4+2P2Q/4qDmwHSoc3jiqOHIdmxwnHDSc0pwOub0wtnEme/c5jznYuOy3uW8K+Lq4Vro2u8m4xbiVun22F3dPc692X3Gw8Jjncd5T7Snt+duz2EvZS+WV6PXzAqrFetX9HiTvIO8K72f+Oj78H26fGHfFb57fB+u1FrJW9nhB/y8/Pb4PfLX8U/z/z4AE+AfUBXwNNA0MDewN4gSFBXUFPQm2Dm4JPhBiG6IMKQ7VDI0MrQxdC7MNaw0bGSV8ar1q66HK4RzwzsjsBGhEQ0Rs6vdVu9dPR5pEVkQObRGZ03WmqtrFdYmrT0TJRnFjDoZjY4Oi26K/sD0Y9YxZ2O8YqpjZlgurH2s52xHdhl7imPHKeVMxNrFlsZOxtnF7YmbineIL4+f5rpwK7kvEzwTahLmEv0SjyQuJIUltSbjkqOTT/FkeIm8nhSVlKyUgVSD1ILUkTSbtL1pM3xvfkM6lL4mvVNAFf1M9Ql1hVuFoxn2GVUZbzNDM09mSWfxsvqy9bN3ZE/kuOd8vQ61jrWuO1ctd3Pu6Hqn9bUboA0xG7o3amzM3zi+yWPT0c2EzYmbf8gzySvNe70lbEtXvnL+pvyxrR5bmwskCvgFw9tst9VsR23nbu/fYb5j/45PhezCa0UmReVFH4pZxde+Mv2q4quFnbE7+0ssSw7uwuzi7Rra7bD7aKl0aU7p2B7fPe1l9LLCstd7o/ZeLV9WXrOPsE+4b6TCp6Jzv+b+Xfs/VMZX3qlyrmqtVqreUT13gH1g8KDjwZYa5ZqimveHuIfu1nrUttdp15UfxhzOOPy0PrS+92vG140NCg1FDR+P8I6MHA082tNo1djYpNRU0gw3C5unjkUeu/mN6zedLYYtta201qLj4Ljw+LNvo78dOuF9ovsk42TLd1rfVbdR2grbofbs9pmO+I6RzvDOgVMrTnV32Xa1fW/0/ZHTaqerzsieKTlLOJt/duFczrnZ86nnpy/EXRjrjup+cHHVxds9AT39l7wvXbnsfvlir1PvuSt2V05ftbl66hrjWsd1y+vtfRZ9bT9Y/NDWb9nffsPqRudN65tdA8sHzg46DF645Xrr8m2v29fvrLwzMBQydHc4cnjkLvvu5L2key/vZ9yff7DpIfph4SOpR+WPlR7X/aj3Y+uI5ciZUdfRvidBTx6Mscae/5T+04fx/Kfkp+UTqhONk2aTp6fcp24+W/1s/Hnq8/npgp+lf65+ofviu18cf+mbWTUz/pL/cuHX4lfyr468Xva6e9Z/9vGb5Dfzc4Vv5d8efcd41/s+7P3EfOYH7IeKj3ofuz55f3q4kLyw8Bv3hPP7BwWMTQAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEwAACxMBAJqcGAAAIHBJREFUeNrtmwdYVFe39xGki12KiEjvvZeh9zIMMMDQYQYYelGQ3gTpXXpRinQQFMFGRGMvMfGNSYwtlhhjYouJGBNl3b0P5QUhvnm/e5/vue9NzvPsZwZmznBm79/+r/9a68DC8vfxH3sYGBqyMcMjJag0d3cnqnO8CdlqO9XdPczPP4BEj0xY+fcM/R8/bO3s2YKSks2TB8s7nWrC7ihm20wZ5XoAX6IKLEuQAckMM9DJcQbDYo9XyX1VJ3suH89gxMQL/T//QS03c155Z70N7CSBDRoephsMPK242Zaz/b0S/83DnmzH611GTQnvY3QzdvtUuAdQRf7VOVxcXCxxcXEG6urqTTFp2X1H7jz7R9/FryF7YgzYtkmAXi4V2LfLAcvckAWpTHPYmKaP4JAF4XQDMMv3+SXz2O4aZS3tdX/qQjfIbuJXibNKkCiwuMiTqjrJlqTwDn34u+WJCu/40tR+kS6wPGecRk0UVZPa9Fdf1LiKNLuAvqTWgP7kBSNiX2athqmu2FLnrFq9all8U2xEzuHMWu92anfhsZz89H2JxcJSwkvuMgEBAW4LCwspGxubenVtnVuxJfVT+649hP7P7kP55U/Bf18NSGZbw+YMEizDECTKo4EekxSANUkedPOoCBb0s8QK4DETAc1cJ1Avcf4hs6WS9sEvl1CTabshU/t7lniZeYQtMeKlYVWaxgv/0hgGOwfHX1YqrJI94vFcsKCdN3+sSVV7Jqoto7Hk4m7mZ9u6Lzwn61BqhH+H+6nd5+sTCo7tSJEkiXLNf5+urp4YIyjoEI3m+ZOmpiaoqKj+llXZcC88twIajl2CoXuvQKuagRZaErR3ukLd1S8h5nAPrEzXAvlKT2BNkQO2VAUQytKHdZsFgI0iAsvSlWB5siJo57rA2mTVKZ+CbVlLfjEq01Nidarqzx+E4L3BuV3+d/twV9JfFQbrZFo8AcB787ImTQPBIKvxR+dtL95mtHVfxDGvnW6usYNhnXn7smLnv66np7cqMDDwWEBAAOjq6gIKD+Dr6wsC/PxAp9PByNwKtlY2g2FDGCgVuUL62ADs+eIuVF+5CrIlDpBzehw25ZPArTcX+KhbQJGqAytiZWFFpgpwICC4UpVBI5cCbNtlpjzzYvMWXWDEgZ0lLAlLKYIscCUr/4QkaGrRa+j95Lbwbu4VPMv/sjBs//dhWL169TKnQEcpLTc1T32ajpaojOiyed6AS0ZGphM9BTY2NkhJSYH09HRYtmwZ4N/xIyBCQ0MhIz0DJANMgVK3DTbtNIf1uQaQfvIQtH15Hw7cfQ1dXz9Cj69AlWQGYaV5sMZIEETyNWF9jhrwIijWZemCeJY5cCYqTKU2lQUuuECFEruLS+1+41qfk7reZjxBfVkVyKUuAkI0z/Tycm4OrkUmycdWkJLh7BbeF1pD7/IfjRkMm4jqZ3QHlXkm2AdaaXDzcn8wvLj4WolvLXL377mQVt97Pu3Q4KXsiZ7zSYMjn5aUhydTXCzt9QTfP4dCMxGlRzr5BkSS/fwiHP38wh39fMIc0KOTO7/gupXOVPM1wenOodUfbS0SV9y4YvY8YWH+ZVSGjYRdlElA3GBQfWRf0KHI/tAJv70+g/QeRqFFjBVF01ZbYBEMKQiGxKVgUH+maKOtqeZmpKiTSdmpU0U9rFnpOqZT5Fag62muLSUnxbrUd+bj42NDCx3Dzs7+Di88hUKB6upqYGVlJUCYHRiM+Ph42KAjAgp59sCWpgCrslRBMEcVDGqdYetoNZjVmAG1iQZiRltASE0ISAUk0CjTA+lCFEbKbBE8OqCR5wScycqwMdvwVUTCVtG5C+FJVbuz2BvIgOEodRsrsgVrTMVpyJW+W/A6Miu86RrXlnEt5579HBllOa7ssfxUnUqTl2olOqBfoQ9m1UZgW2sMlAZTcGsyBd/dFpA1En7CwEZj0e4Rl960tu14dm35Eb835Ue8YNcxX6j7yB8aTwTC7pN0aP2YAW2nGNB9LuJ552j2tvUb1s397ewWslvzSf93jRP4HB90rhdUHPGEiqN+D+r25tjXT8R+kTXsAmlDLpc3ya5bhc9xdncUTGqnNwZ32P/q1WIOrk1m4FhvClY1RmBcRQKdcj1QLtYG7UrzZ8yW2KjNW0Q5/gmDZzxh1t6bt5Wpai89urYNcCcp/bpAbdFztBN/i+rKKJCVlV0EhLu7B4mHh+cNXnBhYWE4cOAAiIiIgLa2NkhKSi4AwtraGowsjUE3nwoiOYYgjcKCegkJTKtMgFyH59gayFl6IK4oCusE+SGqMQic663BssoKnBoCoeSjw+jnOPCsSQCzKiYk7Ssfm7sQoWz9n5cKA57d23OXs7OzCJhL01jjpd9Ny+LMwBORrHCNhZOVWBANTQ0uWhvzgESB5pRyiS5olekDqdIQLBAM9nUm4NJoBp4tFhDQagUhHTaQ0O/+OCaVSVq1ilgXFlkFqbVVo9HnCkc9oOyI9wwIftA4EUCA0H4qCPaeDoGusyEIBiYaIdA/XpK5gX89MbEFrc6uHaeD37ai97Wg9zeg82rG/aDpRMTTPacS7haMukPOATfIHHa9LIJg0CZpCpYdi78YutcWgtptwG+PJbg3m6PJMgUbBK/pLhKC2QDUS/VAHgEhUaD1LrgqOmN2zmxSl4Zhdu7+yGutTlN/7hDnpj0fBFVV1c3IJ3yJVQCFEYiNjYWRkREQExMjwgT2DThszMKAvYQ+yQD0811AOd8SzMocwK3GFUKbvSCxIwB29ocCmWYJJhaGkNzgBfXDLZDYFQyBTa5gt8sGlAr1QBSpRUh7IRx88AaUy2lv/aPDNImLWZOh/c1SFy5fZPu1nI7KChVnfT3Tet89pg1+rQtGY0A+ChPEbqkYb44VyVN/J1OkDaoIBp1yfTBCu8umzvSdT5vzd9Qm83feuy2B3mYNoXvtIKrLASrG488IbNxASHbjweyC/FGPqZJDXlB51IdYSAxCx+mI511nI1/sPTMNQe/5UOi/EAYDF8Ng/ydbn/sGkeXx+YVtLq5dZ5lvERCEgjQjNak/7g/Vx/ygHMFVPOYJOw+6A1KHy0Z2mqtrRrJ2xXQ7QESnPYLTloCUhmD1biU/pbbY/WxWTQJDBLMmglqxWAfDAEpl5g/p8UzpORiS5KfTufcXPF3rxdoMrZdLzSkrStW1cyhBsyCsXLmSFS14XlRUFCC/ACUlJdDX1wdnzpyBtrY22L59O/E7cXHxORjIZDLIaSqBWL4RbEFeAM+1U6MphKF5zd5PhaQGBxDatB4q+71h75lgaB7Nhb3XrqJ5tyeUD2/UjbkqUHX+Auy7/gT6bz2D/i+vthMXpFLmOL404bIgVWD1hZaXuTErG9uyD8V5i2qPQVF0YbIIBjW0m/TQBZpUGULcYHiPtpE259bu4KM+KEQEIRjwRUd3O0LigMvvCbkMLSkFKdaSA8yPC0dpUHZ4OjzghWw5yXiXXR5hlpUfrdl/Ifpp3wwE+y6Fw/DlCGKU1kfb4r9f1O7iikB524mUAwOB1aRxRh0wXBiyAqQ6aLIuu9DNBEuPRJyJ63WESAQlE8EQ2GoNfq22P9sxzA2sfM317estf8LKhidOCcEgieKtaL7WW69SJmUOhuTFMPAgw00KdiDJ2mpqcCYpTS4BAzhXhKTNzhuDwTDIy8v7ub+/Hw4gNWhtbYVz587ByZMn4fnz53DkyBEChpqaGuDg4IDly5eDl5cXqFjpgnCeDjHfZkjF/PZYQco+Fyge8gBdY2UobQyBK980wOnrBdB/IgXaL+yC4oka8GoxBXKzK+SeOgTDd36Bvk/uwPA3r+DIg9ePDEgmHCzkHYFhLNuk/lDa2BAUJrXePWZ0O2l0MYtAUFBRYFMpsfoKUyqHLg5LK5ZYE3SR3u3Uap7V3Kx+tY6NWIqDkSSHo90Y0+MI8X2OwEihhFs5kfgqjgbdwbu3Ai1cNVpALPOtp5i/ZhaHKto5mfIfuJJ0D4MwhAA48EkkjFyJIkZ+DcMLX0Nxu6srUoy3WD2wiuBwgdUBew4MVymCDIegHAQDhW6inD/GuLOtz4lQKKxUWLGC9zo/l1KTUBORF95MabK7bYRg1kYwKCP/g82XSJ7mlF6avd+HYFiVqvZUWFNSQ9vDUJI3RfXp+3OJi0PG+R6p+DMEBQXZOzs790xMnIDBwUE4OHoQMBQ9PT1w8+ZNOH78OPF47NgxiImJAU5OThAQEIDtidtBhWEMQgWaoFKhB3ZYFTrtILPPDdq666GhLQvuP/sYHr24Brcfj8PElQroOBUCTRd6oPB4HfTf/B723XkJwyjz6L3wFfE49M2rqcikdD0WRS3llVIFFneWSpXmqwQ2R3rJTukyekqc82HQcNbjki81eSSGLk5+Hgw47vp1e9bwrOFhpZSR6vxnYMDSHNdLRr6BDLltUZm0QCehyqNBj0oOeRK7uHbGK7SfDnuzsyJG0dHZkn/0s9R7WBEwCAevRMPYpzHEyK8OjCFg6PByHbwY9haHEawO7fO8QzWCoeywJ0z7BpfLlt5q6hnDtHvxCAasUFipGAgG5l6X59Lqkmqiips3u7Y43p42kfqggsKedBGGQQM8SgLDPwTDXGq5nlMM/fz4QzCsXbuWGy30sbt378J3330HN27cgJ07d0J3dzecOHkCOjo64M6dO4SZLCwsJJTB0dERNLQ1YcMWQXBj0kDeUglEDITA0E8BgsLowIxxhs/u7oYffvoaXv/2Cr5HQJz7sgV6zoZDyZFgSB/NQCHFFnpu/IAUYRJ6L95AMExiGMB/ayqDWFCnEJrMxh2G9z9kfmYrkKplTqfl9FXmStJSFmpckkWkR+IzMGggGAwQDGYLYahCcXkSxedJBMMkgmESwTB59IumAmas18bKo4xHWMqrjvoiGPyR8QuEjjNhv+2silXMzo/WGv4k7ikOC1gNMASHP4uFI1fjILPcKQpfwy4EA4LlLQ4lyDvMhQoMQ/1x+suK/sg8t2Bze/8IFw1jBw2ZzGHa/YR+J4hBMIQjGILabIDZ6fICw2Drb0VyarR+MQsD9kDYC23O0wSNZKs/B8OGfw3DmjVrOBwcHAaoVCp0dXXBrVu3IDo6GjAco6Oj8OOPPxKAPHnyBHJycgC/r6ioEFgc+YHfVBKs7ayAGRwCGWnpILxRGKRVhGHvqWC4/E0VPHxxFp6+ugF3Hk/Ax58jZTgZBjkj7hCMzHtAVwgcuPuGCA/9V6bDBFYH77jUqLkdLqYitda6gd65HJmcD1cgkZcotHyoZqS1+X0YFIqXhkHCQoxdjSzJre4sya3rJsmt5yaPhiy3kp44u5S8OJtLkPlmZ4aJmFuwsZh7sJFYfnX0lnO3Ok2Of1FRjfzCMxwi5sOAQTj8WQwU1QYTVbTqGRj634fhuP9U25GCGMFNgnOeR0B43XJyAEnE1k9PzMpXV8zSR0fMPd5RvO7jEtO8w6nN1GbrF/9MLxfCIBKtsRCGJIV/GwYjBMOKFSvYkXHMRYv+29jYGFRUVBAgYH/w+PFjwj9cuHABGhoa4OrVq1BeXg4mpqZAMjOD8KE9UDB+CLqufw9x3S2gE2oKCmRR2NbpANElliAivhbklDZDVrE/XLhVCsPn46H2cDCkDriBf4sNVJ0+iozjUxi89gi6T11FoeJr6Dn/FXgyoxMXeQC3nSHWsoXWX3ywT4HSKqe28APcK3jY/0gZ5oeJP1vZc/W14U8u8WX2nE+8iOL+u47TQUQWMYAWeQiFiZFPomAUwXAIKcPYp9FQWBNSis8rx2GCUIZQAoZ2BAMOE80ng15XNifq/NHfM3cyEKYl2ESkDtE/9d1j9W5xrWE6TEzDoAFrQmX/28pAKvBIpbnTFLOzs1950GggJCQESUlJwMPLCxYWFgQQODTcv38fwsLCiPTy1KlTkJqSCjHpO2Do8wfQffIK9F2+Cf2XbkHt+BGUVvoDPYsK2ubykFXNhLxaJvQcT4PWw4lQN7Qd8nsjIKkzFHafPwdDKIMYuvEchm799L4ybFtyksSUpTiVY8wzeJOVXy/pJdCOWJOt/ZNNcowAhkGikPRIbAkD+WdhkFWQ4hk4WZRadzzkWfUxn7n6At7h3edCEQwRMDhPHbBvOHglEgpqgkunDaS7K1KPtz3YMyAD2TZjIBsn6E/E5QVkFzWNhNfzVQwlZSf0uz0P6bAmPANOfakIBnKD2RRKiaeMFnkGzf8RGLAymJma0lCIAAQEtLe3w/r164m0EfsCZCwhIyMDSktLQVxCHIWGIqDTGRCLTCQtgAFd//gWLeI3xALieL/72i3QL9YFAcW1YBmuChLqApDcSSayp9wRN8hAKWZ4uyMUnGhH4eHX6cWfGX2XbhKP2DP4xKWGssSPFqWY1C+sIZjU+7bmH6kLtmY4a0jnW95aBASWxxR5UPE1thO3UOYSLzR8NJtNEKklgsEYuXHfGRhWb1nBpkSW4FZHoUKTisMEHjJEmMCT2zCSUYHc/hSuCVTPpZZ0ZAYjnzZ1Zwcd/DTtQT/a9YMzaeV+ZCT3Xw6HvOqgGRhcXPtQajkbIvZ8zCB8R9NE8BNJBWG5+SCIiIgsKx1IqI/tcZjOJlBqibMJrxYLoHe4PImuCItw30O5Q5qXTUihbGLT/xAMzo3hqUmJSTRbW1tITU0FJpNJ1BhwJnH27FmiyGRkZESogo6ODiRu3w7M8HBwC99K9CaKmvfC4Gf34MD9X4nRcOYaGFLMgE+IFzhXcoBZnBqE7LUlsiRs2Jm7qUCpt4A9n3+1AAQMUu+F67PZBATHp9mxWDcGTkx34OYNZCS9e+I72TnYWeXJOtYovfx9EQxoMkRcVaJmYdicr0HkvTjG6qIdNR8GSWNhO5/dli/QDpxE7n0yutsBmUiHySPXGgsiY+icNeMR1zDJOAWcNpE4vfSHlCL/SCdXG4H9V1Lu9aBwMVtwwioxcJEJudWM0tmiEwonb/fOqMJsJtEwEbQIBg19xXX5Y0GfE3WGeUUnzxbLqfja6CAxRVGUTTjdNpypMyjO1Bk27dSA1aEy4R8qOv0ZGMi7glPRIpfgRa+srITk5GRQU1ODp0+fwuzx7Nkz6O3theCQEPAIjYH6j6/BEEoHDWwcwcLaBoo6D0BCSQ3EJqYAPYQJJJoDCAdLg1qOFphWk8C61phoA+Bw515DRethBB1fPSAgwM2saSBwNvE1AcXI3cnXMakZa1hsmxkTS6WSPj0JBAwkmqUwd4rq5Pu9iWUpisBmJRSt52zMJVtq9ginXjgfx+Vo7XJcjjYAn65pGMSNhR3Qzvs1EE06LvLgYg9ejJjdnllUuqNQyWHGo7yD7jBXa0Dq0DBB/z2rJEILwzB0Ofle50wVEocCpALQez4EcqroMzA4uyIQ3uKUcrbghIGqO854IqGwcQEMcppiEmkom8C1DlzzwLsHp71+bfYvjKMN1cUUt2x2aSHfxr4H+x+FmQrkxkUwyP1x1/IDMLjXRZV7uHtcpNFouUgBnuPG08aNG4law8OHD+eA+Oabb8DDwwN9n8+IBduPRtnIx+BApkAqyiB8wmOgZP9J6Lv+DJJO7IO12UogitRLplCbAFgJKRq+dodyGogV6BCGs/XLe2DTlggH771BnuEFDH7xiABj/NvJw8TkbMk3b1sKBpf26M7l7OysK0gbuVgT5Z+9DwNLsgIst94UjYtO8kUWXwmjycI7CF8ILuPiyfTq9CBgCMjwotFaLH7z32M1V3iK6rIHz3irDEuKoVDhWMAj3DvAVcjZknTD8aA3WeXhihiGwUtJ99qI/kQw4Qk6z+LiUhDsqJyGIbfV2RUZzrcECPOKTdXj9CcS8gthkFbfLJ406HZvtuCE/QJSLQjscCbqDGJKWzY7N5Nv61XoEyEPhz4cAgVzNWAVU3oahj9oVK3FMOgQMGxZthQM6BxqVXh/QnzCJQkJCT4nJ6feELT7eZF5xCEBZw2NjY2EWuAehLq6OqTWtsP++9OxHgMxdOcX6L5yD4avP57+Hdrhzj15wJGuCHxZqrB2hxpsyFEHfjRw29qi2BOovTlQfOEMcKHQHjTSjD7nNfQh3zF0+yUKEZNTe/YfnS6R+/UkFC9VX9CocB1jY1/OZkizUOFMUnyzOKOQBQl3TSb+DKNKtzGhXHUgCk/F0yYST2ZQD6OFby0fW2ZPUoRbs/mUL5p0xkx/IrLTDgKTnX0snAyE8kb9H+G6OlaHorHpsnT1eMBvaYVMNRuyEX/n2bh7WPr3EJ3LIKKo1HqKDlmVgQQMmXsori0nAt9iRagjehK+UIH8R+XRgEUwSKqJiCf0U+/N70tgv+C5x+65qNImNRGFTaL2jXZ3FmYSmiCAvh9fiFT4h7qWPMkqT9eqiGgkFqRL8aWqvVgEQ6gkiGnJQuue1u/09PSUzM3NP8Lt6qamJsA3tCgpKcGqVavmGlN+fn5IAWKJ1A87/4HP7qPd/D0Mfvk99H1yG4HxMwEDvgUOrwcrWmx8lxN7GhrpCsCRpgjR3btg7MFbcNqbDVzb5aH83HniMzqPXyQykpHPH9zQMzQmvBtLdHWq3VLlaM4kpd8Uiu0PieQa314qo2BNUgTTYEcibQvoiMsWQCTi/sT8ZpVzC+WBHdOWEtnJmFjYubSF7QNeP1qSTbY4Um025I8yvs0YcoUdSB3yZ8IF9g+VvbFl6WUhYY0TjJfYA2BTiLMEDEbTCX/IKPcnYMiod3JFpvMtDg1YESpm+hFlR/xRNrEQBiVtGfHkfV73cOURdywxoLhj6dxoNpXWtb2IHGsfY1Zt9lpzXl8C+wW821YEz8Kw9M0tHEmKv4e0p+7w3BWVzJ6k+HYRDJHSIG2jhusKDxUVFeWlpKWvqKiogKenJ9Ggwk2o2YbUmjVrICkxEap27YLE9CzI6RyFYSztX/1ALCYGZPDLxzDw+UOkFLdAO98NFLLtQTnHEVRznUAJParlkKH140swiDIQvVJvsKyNhOGvfkRZxA30GY8JVeg4cMxjbnK2JSWsEs8z+/bfue0ND9Uyxy/U7Gx4iAqmj4uMdLHJYzxp2HkrzYYK5BtMqgyQoTECSoMZMel48rF3yGndmse3ko9VWkGcNWvA/0zKoDNkDlOJVnP+QQ+kEJ6AS9RlRzyhipB83xkf4E9kG7Uf+UJKqR8BQ3KdkytSg7dVCALcAsfn4ZBTdMj3iZi80EIDaaDMm30w+DyuxgXOqMJ0fcEELKsNCa+DDfBsiMBqJ4S+1zokvzyzMPzBbW9zLewE6SVfY02QAxlbNdx4esZgMPTl5OTStmzZ8srV1RX4+PggKCgIhDcJ/y4uLn4LKcV139ikl5vEJCAmOhoaW9uh49Nv57IBwvzNPMfhw6QpZqZ9Pp0EsKJr8GrdAcM3f5pOI289g4Hb0897cUqJzrn4/WTvojxf39NSnT9T57t/WY6e+bIbMnW/cwn3Uufk/GebgpLm7SleqPfLFpRVyM2FCgOieINdrQOabBc06bRmM6iayOjmF1rPM3tu/cGdiYkDlN9T97lA5v5pIHDIwAtaeTTwRc1Hwc+w7OPFxjsfj8qjXpBY7EvAEF9r74rS0rdYTbCq4Mxk5wj+DO8nYnILYUATzVJ7MH9nYKvl1KwqUJBq2dYZA25d29Zb/2RaY/lkukE1HSIEUYhYna0KnAtgWDxX3CkqvwplG/y45Dyi32kXul4wN7VIKSkpeebr61vDwcHBamNjG29hYfFYS0uLUAMpKeknAQGB+iv4+JYNnrwQ2PWPh69Ck7OBhNLN9OY+It7jBcWKgJVh2je8grKLl4ADAxovCbwpamDeFIv81VVi0eenk7i2sA8BcvzBq88NSEZL/6ONpKqsiE0Ls3RdhvZT4svMv/uXeI4h0PnRa8+2EgktuSX/KUPHzUiT0kIfVikjTSoiINRKtNEu00NAGIBdnckUo4t2NblpaxA3LxfHAlMnJ768fjw3aceI7w/JgxRI20fB9x5A5bGQu4PjLY4lw8Hjlcfok1Xj9Mld44zJajSqjgVOJpUEFBI3mjaQKQian8uPBk6WHvafLBrzm8wf9Z0sPMT4Vkxu46Ki0yZRYb7CwYwsegflsXOjCUrDSGCBVIHW6vY1bSfNzrjGaly70nRSucx4UraYNClWaDi5MU9/ck2oEuGRHNN9YlenaUyuydCaG6vTNSeNa7zPZNYXylg3BA3ikEHMGxpcycpvNAtdmvg2rV9vamoqEBMb+4+YmJjHdDpdFX+emZmZK/IQx+0dHIb19fXnrhcBwdY7cYE5cP3xa1EpWcjvP/rPhUV+oY8oPk3OqUPl5U+h4vIVGLj1HEaQl8C31c8HYeDqAxhA4QGBcEXXgPSv/+Vho/wWHkmyhpGgjRyN20TEj8d0s5+wnYKnlJMmSUpDfsWfKSubelmt06XpG+nR9Mk6HjpOOu7a1pa+JlJcPJwfrEa6eJBXOjPMTZzoJmS3YEsTE2t9PhRbWcK2+nAytrpxLxgxbtw2ZBPC+PgxfNgYce5zrwVGunH7RLhx+0V4cm0Q2PCH92LoOeiuMvE2MDH01HOy9DM3NrE05l2/fj0LxZ/CSWaQuWeHbSCZ28yHzK1HJhE9fGMHs+Um6Of3hwHFirge3tV8rLqeFrJbHFTsxRxV7VQc9MXn7hF1cNBEqkBH4eExiUQKn//PMoKCgouudcMGfo6MvIJuKXlFaENpJs4A5kIFiv24rLygmDT32k0YuvliDoT+q/eh//Pvpk48nOzT0DNcx/L38b/jQGmjFFIIakJCgtGH3odSTtatW7eGx8XF/R4QxBw9+/jNAJL534nwgNNMBMZs/J8PAoagf1Y1UCqKW9UHvv7xUd3gYX8dEon17xX4DzuQoVwWGxvrinzES5R1XI+IiEjm4eVdFr+jUPujmz90Dl259QxL/sAXj4hdP9urIAC5cJ3wBSgVnTp45eblqt6DCWq6+rx/z+p/6IEyDQVNTc2bUVFR2aGhoQ7W1tYLdrSxpe3KkJh4x7GJMwX7J84eGDr9ybWRT27cHjx64szB05d255ZURQfHblP+eyb/ww9vb299BUXFH5SVlR8iZbAwt7D4e1f/VQ8hoY2eSA0ikDKooUPk/9ff/S9FxinhVCiiHAAAAABJRU5ErkJggg==';

        //get external JSON file
        $.getJSON("sampleData/publishedApplication_florecal1.json", function (data) {
            $scope.getPublish = data;
        });

        ////////////////////////////////////////////////// PDF GENERATION //////////////////////////////////////////////////
        $scope.openPDF = function () {
            console.log($scope.getPublish);
            //generate each-card object to be printed in PDF document
            var eachCard = [];
            $scope.getPublish.forEach(function (v, index) {
                //get lances and nozzle-lances
                var kitLances = [];
                angular.forEach(v.kit.nodes, function (pump) {
                    angular.forEach(pump.nodes, function (tank_tubes) {
                        if (tank_tubes.equipmenttype === 'tube') {
                            angular.forEach(tank_tubes.nodes, function (lance) {
                                var lanceNozzles = [];
                                angular.forEach(lance.nodes, function (nozzle) {
                                    lanceNozzles.push(nozzle.name);
                                });
                                kitLances.push({
                                    text: lance.name + ' (' + lanceNozzles.join(', ') + ') \n',
                                    style: 'subheader'
                                });
                            });
                        }
                    });
                });
                //rowGenerator generates a MEGA row divided by two:
                // columns 1, 2, 3 with tank/products and values (ph, hardness, pressure)
                // other columns like this: map/sector, sector approach, sector beds, sector values, sector velocity and sector times
                var mapsList = [];
                var mainRowGenerator = [];
                angular.forEach(v.map, function (map) {
                    //pH, hardness and pressure
                    var ph_hardness_pressure = [];
                    ph_hardness_pressure.push(new Array({
                        text: $filter('sigFigs')(v.tankmix.ph, 3) + '\n',
                        style: ['tableContent', 'boldText', 'rightalign']
                    }));
                    ph_hardness_pressure.push(new Array({
                        text: $filter('sigFigs')(v.tankmix.hardness, 3) + ' ' + 'ppm' + '\n',
                        style: ['tableContent', 'rightalign']
                    }));
                    ph_hardness_pressure.push(new Array({
                        text: $filter('sigFigs')(v.tankmix.pressure, 3) + ' ' + 'psi' + '\n',
                        style: ['tableContent', 'rightalign']
                    }));
                    //retrieve tank-mix/product-mix with water quantity and quantities (cols 1, 2, 3)
                    var productsAndQuantities = [];
                    var eachProductQuantityRow = [];
                    angular.forEach(map.volumeTank, function (vt, index) {
                        if (($filter('sigFigs')(vt, 3)) > 0) {
                            if (index === 0) {
                                eachProductQuantityRow.push({
                                    text: v.tankmix.name,
                                    style: ['tableContent', 'boldText']
                                });
                            } else {
                                eachProductQuantityRow.push({text: ''});
                            }
                            eachProductQuantityRow.push({
                                text: $filter('sigFigs')(vt, 3) + ' ' + 'l',
                                style: ['tableContent', 'boldText', 'rightalign']
                            });
                            productsAndQuantities.push(eachProductQuantityRow);
                        }
                        eachProductQuantityRow = [];
                        angular.forEach(v.tankmix.productMix, function (p) {
                            if (($filter('sigFigs')((p.concentration * vt), 3)) > 0) {
                                var product = {text: p.product, style: 'tableContent'};
                                var quantity = {
                                    text: $filter('sigFigs')((p.concentration * vt), 3) + ' ' + (p.unit ? $filter('convertUnits')((p.unit)) : ''),
                                    style: ['tableContent', 'rightalign']
                                };
                                eachProductQuantityRow.push(product);
                                eachProductQuantityRow.push(quantity);
                                productsAndQuantities.push(eachProductQuantityRow);
                                eachProductQuantityRow = [];
                            }
                        });
                        eachProductQuantityRow.push({text: ' ', style: 'tableContent'});
                        eachProductQuantityRow.push({text: ' ', style: 'tableContent'});
                        productsAndQuantities.push(eachProductQuantityRow);
                        eachProductQuantityRow = [];
                    });
                    if (map.volume > 0) {
                        eachProductQuantityRow = [];
                        eachProductQuantityRow.push({
                            text: '¡Advertencia! Los tanques de este kit no son lo suficientemente grandes para la cantidad requerida.' + ' ' + $filter('sigFigs')((map.volume), 3) + ' ' + 'l',
                            style: ['tableContent', 'red']
                        });
                        eachProductQuantityRow.push({
                            text: ' ',
                            style: 'tableContent'
                        });
                        productsAndQuantities.push(eachProductQuantityRow);
                    }
                    mainRowGenerator = [];
                    mainRowGenerator.push({
                        table: {
                            widths: ['*', '*', '*'],
                            margin: [0, 0, 0, 0],
                            body: [
                                [
                                    {
                                        table: {
                                            widths: ['*', '*'],
                                            margin: [0, 0, 0, 0],
                                            body: productsAndQuantities
                                        }, colSpan: 2, layout: 'noBorders'
                                    },
                                    {},
                                    {
                                        table: {
                                            widths: ['*'],
                                            margin: [0, 0, 0, 0],
                                            body: ph_hardness_pressure
                                        }, layout: 'noBorders'
                                    }
                                ]
                            ]
                        }, colSpan: 3, layout: 'noBorders'
                    });
                    mainRowGenerator.push({});
                    mainRowGenerator.push({});
                    //map-related data (other cols)
                    var times_lastColumn;
                    if (map.sector) {
                        times_lastColumn = {
                            text: moment(map.sector[0].startplanned).format('HH:mm') + ' - ' + moment(map.sector[map.sector.length - 1].endplanned).format('HH:mm'),
                            style: ['tableContent', 'rightalign']
                        };
                    } else {
                        times_lastColumn = {text: ''};
                    }
                    var mapSector_allRows = [];
                    var mapSector_rowGenerator = [];
                    mapSector_rowGenerator.push({
                        text: map.name,
                        style: ['tableContent', 'boldText']
                    });
                    mapSector_rowGenerator.push({text: '', style: 'tableContent'});
                    mapSector_rowGenerator.push({text: '', style: 'tableContent'});
                    mapSector_rowGenerator.push({text: '', style: 'tableContent'});
                    mapSector_rowGenerator.push({text: '', style: 'tableContent'});
                    mapSector_rowGenerator.push(times_lastColumn);
                    mapSector_allRows.push(mapSector_rowGenerator);
                    mapSector_rowGenerator = [];
                    angular.forEach(map.sector, function (sector) {
                        //sector name
                        mapSector_rowGenerator.push({text: sector.name, style: 'tableContent'});
                        //sector approach
                        mapSector_rowGenerator.push({text: sector.approach, style: 'tableContent'});
                        //sector beds
                        mapSector_rowGenerator.push({
                            text: sector.bed ? $filter('bedsFilter')(sector.bed, 'es') : '',
                            style: 'tableContent'
                        });
                        //volumes and strata
                        var totalVolume = $filter('sigFigs')((sector.volumetop + sector.volumemid + sector.volumebase), 3);
                        var strata = $filter('sigFigs')(sector.volumetop, 3) + " - " + $filter('sigFigs')(sector.volumemid, 3) + " - " + $filter('sigFigs')(sector.volumebase, 3);
                        mapSector_rowGenerator.push({
                            alignment: 'center',
                            columns: [
                                {width: '60%', text: strata, style: ['tableContent', 'rightalign']},
                                {
                                    width: '40%',
                                    text: totalVolume + ' ' + 'l',
                                    style: ['tableContent', 'boldText', 'rightalign']
                                }
                            ]
                        });
                        //velocity
                        mapSector_rowGenerator.push({
                            text: $filter('sigFigs')(sector.speed, 2) + ' s/' + 'cama',
                            style: ['tableContent', 'rightalign']
                        });
                        //empty row
                        mapSector_rowGenerator.push({text: '', style: 'tableContent'});
                        mapSector_allRows.push(mapSector_rowGenerator);
                        mapSector_rowGenerator = [];
                    });
                    mainRowGenerator.push({
                        table: {
                            widths: ['*'],
                            margin: [0, 0, 0, 0],
                            body: [
                                [{
                                    table: {
                                        widths: ['*', '*', '*', '*', '*', '*'],
                                        margin: [0, 0, 0, 0],
                                        body: mapSector_allRows
                                    }, layout: 'noBorders'
                                }]
                            ]
                        }, colSpan: 6, layout: 'noBorders'
                    });
                    mainRowGenerator.push({});
                    mainRowGenerator.push({});
                    mainRowGenerator.push({});
                    mainRowGenerator.push({});
                    mainRowGenerator.push({});
                    mapsList.push(mainRowGenerator);
                });
                //conditional page break
                var conditionalPageBreak = 'after';
                index < ($scope.getPublish.length - 1) ? conditionalPageBreak = 'after' : conditionalPageBreak = 'none';
                //final card
                var card = [
                    {text: ' ', style: 'subheader'},
                    {
                        table: {
                            widths: ['*', '*'],
                            body: [
                                [
                                    {text: "Kit", style: ['subheader', 'boldText']},
                                    {text: v.target.name, style: ['subheader', 'rightalign']}
                                ],
                                [
                                    {text: v.kit.name, style: 'subheader'},
                                    {
                                        text: moment(v.map[0].sector[0].startplanned).format('YYYY-MM-DD') + '',
                                        style: ['subheader', 'rightalign']
                                    }
                                ]
                            ]
                        }, layout: 'noBorders'
                    },
                    {ul: kitLances},
                    {text: ' ', style: 'subheader'},
                    {
                        style: 'tableExample',
                        table: {
                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*'], //columns width equivalent to width: 100%
                            headerRows: 1,
                            body: [
                                [
                                    {
                                        table: {
                                            widths: ['*'],
                                            margin: [0, 0, 0, 0],
                                            body: [
                                                [{
                                                    text: 'Mezcla tanque',
                                                    style: ['tableHeader', 'boldText']
                                                }],
                                                [{text: 'Producto', style: 'tableHeader'}]
                                            ]
                                        }, layout: 'noBorders'
                                    },
                                    {
                                        table: {
                                            widths: ['*'],
                                            margin: [0, 0, 0, 0],
                                            body: [
                                                [{
                                                    text: 'Cantidad de agua',
                                                    style: ['tableHeader', 'boldText', 'rightalign']
                                                }],
                                                [{
                                                    text: 'Cantidad',
                                                    style: ['tableHeader', 'rightalign']
                                                }]
                                            ]
                                        }, layout: 'noBorders'
                                    },
                                    {
                                        table: {
                                            widths: ['*'],
                                            margin: [0, 0, 0, 0],
                                            body: [
                                                [{text: 'pH', style: ['tableHeader', 'boldText', 'rightalign']}],
                                                [{
                                                    text: 'Dureza',
                                                    style: ['tableHeader', 'rightalign']
                                                }],
                                                [{
                                                    text: 'Presión',
                                                    style: ['tableHeader', 'rightalign']
                                                }]
                                            ]
                                        }, layout: 'noBorders'
                                    },
                                    {
                                        table: {
                                            widths: ['*'],
                                            margin: [0, 0, 0, 0],
                                            body: [
                                                [{text: 'Bloque', style: ['tableHeader', 'boldText']}],
                                                [{text: 'Sector', style: 'tableHeader'}]
                                            ]
                                        }, layout: 'noBorders'
                                    },
                                    {
                                        table: {
                                            widths: ['*'],
                                            margin: [0, 0, 0, 0],
                                            body: [
                                                [{text: ' ', style: 'tableHeader'}],
                                                [{text: 'Enfoque', style: 'tableHeader'}]
                                            ]
                                        }, layout: 'noBorders'
                                    },
                                    {
                                        table: {
                                            widths: ['*'],
                                            margin: [0, 0, 0, 0],
                                            body: [
                                                [{text: ' ', style: 'tableHeader'}],
                                                [{text: 'Camas', style: 'tableHeader'}]
                                            ]
                                        }, layout: 'noBorders'
                                    },
                                    {
                                        table: {
                                            widths: ['*'],
                                            margin: [0, 0, 0, 0],
                                            body: [
                                                [{
                                                    text: 'Litro por metro',
                                                    style: ['tableHeader', 'boldText', 'rightalign']
                                                }],
                                                [{
                                                    table: {
                                                        widths: ['*', '*'],
                                                        margin: [0, 0, 0, 0],
                                                        body: [
                                                            [
                                                                {
                                                                    text: 'Tercio',
                                                                    style: ['tableHeader', 'rightalign']
                                                                },
                                                                {
                                                                    text: 'Total',
                                                                    style: ['tableHeader', 'boldText', 'rightalign']
                                                                }
                                                            ]
                                                        ]
                                                    }, layout: 'noBorders'
                                                }]
                                            ]
                                        }, layout: 'noBorders'
                                    },
                                    {
                                        table: {
                                            widths: ['*'],
                                            margin: [0, 0, 0, 0],
                                            body: [
                                                [{text: ' ', style: 'tableHeader'}],
                                                [{
                                                    text: 'Velocidad',
                                                    style: ['tableHeader', 'rightalign']
                                                }]
                                            ]
                                        }, layout: 'noBorders'
                                    },
                                    {
                                        table: {
                                            widths: ['*'],
                                            margin: [0, 0, 0, 0],
                                            body: [
                                                [{
                                                    text: 'Hora',
                                                    style: ['tableHeader', 'rightalign', 'boldText']
                                                }],
                                                [{
                                                    text: 'Inicio' + ' - ' + 'Final',
                                                    style: ['tableHeader', 'rightalign']
                                                }]
                                            ]
                                        }, layout: 'noBorders'
                                    }
                                ],
                                [
                                    {
                                        table: {
                                            widths: ['*', '*', '*', '*', '*', '*', '*', '*', '*'],
                                            margin: [0, 0, 0, 0],
                                            body: mapsList
                                        }, colSpan: 9, layout: 'noBorders'
                                    },
                                    {},
                                    {},
                                    {},
                                    {},
                                    {},
                                    {},
                                    {},
                                    {}
                                ]
                            ]
                        },
                        pageBreak: conditionalPageBreak,
                        layout: {
                            hLineWidth: function (i, node) {
                                return (i === 0 || i === node.table.body.length) ? 0 : 1;
                            },
                            vLineWidth: function (i, node) {
                                return (i === 0 || i === node.table.widths.length) ? 0 : 1;
                            },
                            hLineColor: function (i, node) {
                                return (i === 0 || i === node.table.body.length) ? 'black' : '#C1C1C1';
                            },
                            vLineColor: function (i, node) {
                                return (i === 0 || i === node.table.widths.length) ? 'black' : 'white';
                            }
                        }
                    }
                ];
                eachCard.push(card);
            });
            //PDF document
            var docDefinition = {
                pageSize: 'A4',
                pageOrientation: 'landscape',
                pageMargins: [40, 40, 30, 40],
                styles: {
                    header: {
                        fontSize: 22,
                        bold: true,
                        margin: [10, 10, 10, 10]
                    },
                    headerElements: {
                        margin: [50, 0, 10, 0]
                    },
                    subheader: {
                        fontSize: 9,
                        bold: false
                    },
                    rightalign: {
                        alignment: 'right'
                    },
                    tableExample: {
                        margin: [0, 0, 0, 0]
                    },
                    tableHeader: {
                        fontSize: 9
                    },
                    tableContent: {
                        fontSize: 7
                    },
                    boldText: {
                        bold: true
                    },
                    red: {
                        color: '#ff0000'
                    }
                },
                header: {
                    style: 'tableExample',
                    margin: [38, 15, 30, 0],
                    table: {
                        widths: ['*', '*'],
                        body: [
                            [
                                {text: 'Florecal I', style: ['subheader', 'boldText']}
                            ]
                        ]
                    }, layout: 'noBorders'
                },
                content: [eachCard],
                footer: function (currentPage, pageCount) {
                    return {
                        margin: [38, 0, 30, 0],
                        columns: [
                            {image: $scope.scarabLogoBase64, width: 75, height: 20},
                            {text: currentPage.toString() + '/' + pageCount, style: 'rightalign'}
                        ]
                    }
                }
            };
            pdfMake.createPdf(docDefinition).open();
        };
        ////////////////////////////////////////////////// PDF GENERATION //////////////////////////////////////////////////
    });
});
