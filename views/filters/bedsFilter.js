/**
 * Created by Sergio A Guzman <sagg629@gmail.com> on 11/03/16 11:59 PM.
 */
define(['app'], function (app) {
    app.filter('bedsFilter', function () {
        var parseGroups1 = function (groups, lang) {
            var result = [];
            for (var i = 0; i < groups.length; i++) {
                if (groups[i].length === 3) {
                    if (groups[i][2] === 1) {
                        result.push(groups[i][0] + '-' + groups[i][1]);
                    } else if (groups[i][2] === 2) {
                        if (groups[i][1] - groups[i][0] === groups[i][2]) {
                            result.push(groups[i][0]);
                            result.push(groups[i][1]);
                        } else {
                            if (groups[i][1] % 2 === 0) {
                                result.push(groups[i][0] + '-' + groups[i][1] + translate[lang][0]);
                            } else {
                                result.push(groups[i][0] + '-' + groups[i][1] + translate[lang][1]);
                            }
                        }
                    } else {
                        for (var j = parseInt(groups[i][0]);
                             j <= parseInt(groups[i][1]);
                             j += parseInt(groups[i][2])) {
                            result.push(j);
                        }
                    }
                } else {
                    result.push(groups[i][0]);
                }
            }
            ;
            return result.join(', ');
        };

        var grouping2 = function (selection) {
            var intermediate = [],
                diff = [],
                step, min = -1;
            for (var i = 0; i < selection.length - 1; i++) {
                step = selection[i + 1] - selection[i];
                diff.push(step);
            }
            ;
            step = diff[0];
            diff.push(step);

            var j = 0;
            var step = diff[j];
            var group = [parseInt(selection[j++])];
            for (; j < diff.length; j++) {
                if (diff[j] === diff[j - 1]) {
                    group.push(+selection[j])
                } else {
                    intermediate.push({"step": step, "group": group});
                    step = diff[j];
                    group = [parseInt(selection[j])];
                }
            }
            intermediate.push({"step": step, "group": group});

            for (var i = 0; i < intermediate.length - 1; i++) {
                var l1 = intermediate[i].group.length;
                var l2 = intermediate[i + 1].group.length;

                if (l1 >= l2) {
                    if (parseInt(intermediate[i].group[l1 - 1]) + parseInt(intermediate[i].step) === parseInt(intermediate[i + 1].group[0])) {
                        intermediate[i].group.push(intermediate[i + 1].group[0]);
                        intermediate[i + 1].group.shift();
                    }
                }
            }
            var results = [];
            for (var i = 0; i < intermediate.length; i++) {
                var length = intermediate[i].group.length;
                switch (length) {
                    case 0:
                        break;
                    case 1:
                        results.push([intermediate[i].group[0]]);
                        break;
                    default:
                        results.push([intermediate[i].group[0], intermediate[i].group[length - 1], intermediate[i].step]);
                }
            }
            return results;
        };

        var translate = {
            "es": [' pares', ' impares'],
            "en": [' even', ' odd']
        };

        return function (beds, language) {
            return parseGroups1(grouping2(beds), language);
        };
    });
});