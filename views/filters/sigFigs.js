/**
 * Created by Sergio A Guzman <sagg629@gmail.com> on 11/03/16 11:47 PM.
 */
define(['app'], function (app) {
    app.filter('sigFigs', function () {
        return function sigFigs(n, sig) {
            if (n !== 0 && n !== undefined) {
                var mult = Math.pow(10,
                    sig - Math.floor(Math.log(n) / Math.LN10) - 1);
                return Math.round(n * mult) / mult;
            } else {
                return 0
            }
        };
    });
});
