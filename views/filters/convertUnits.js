/**
 * Created by Sergio A Guzman <sagg629@gmail.com> on 11/03/16 11:50 PM.
 */
define(['app'], function (app) {
    app.filter('convertUnits', function () {
        return function (input) {
            var exp = /^\w+/;
            return exp.exec(input)[0] === 'undefined' ? undefined : exp.exec(input)[0];
        };
    });
});