/**
 * Created by Sergio A Guzman <sagg629@gmail.com> on 3/03/16 03:18 PM
 */
define(["app", "views/directives/preloaderDirective"], function (app) {
    app.controller('view1Controller', function ($scope) {
        $scope.msg1 = "view 1";
    });
});

