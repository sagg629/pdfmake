/**
 * Custom directive to generate materialize preloader element,
 * it receives a size parameter that defines size of the preloader:
 * {big, normal, small}
 * http://www.ng-newsletter.com/posts/directives.html
 * http://stackoverflow.com/a/24918404/3135458
 *
 * Created by Sergio A Guzman <sagg629@gmail.com> on 11/03/16 03:21 PM
 */
define(['app'], function (app) {
    app.directive('preloader', function () {
        return {
            restrict: 'E',
            templateUrl: 'views/directives/preloaderTemplate.html',
            scope: true,
            replace: true,
            link: function (scope, elem, attrs) {
                scope.tAttrs = attrs;
            }
        }
    });
});
